import React, { createContext, useContext, useState, useEffect } from "react";
import { sendJsonRequest } from "./Utils";

const UserContext = createContext(null);
export const UserProvider = ({ children }) => {
	const [user, setUser] = useState(null);

	useEffect(() => {
		(async () => {
			try {
				const response = await sendJsonRequest("checkUserStatus");
				setUser(response);
			} catch (error) {
				console.error(
					"Błąd podczas pobierania statusu użytkownika:",
					error,
				);
			}
		})();
	}, []);

	return (
		<UserContext.Provider value={{ user, setUser }}>
			{children}
		</UserContext.Provider>
	);
};

export const useUser = () => useContext(UserContext);
