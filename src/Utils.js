const { UserRole } = require("../src/shared/shared");

async function sendJsonRequest(endpoint, data = {}) {
	const requestOptions = {
		method: "POST",
		mode: "cors",
		redirect: "follow",
		credentials: "include",
		headers: {
			"Content-Type": "application/json",
		},
	};

	if (Object.keys(data).length !== 0) {
		requestOptions.body = JSON.stringify(data);
	}

	const response = await fetch(
		`http://localhost:5000/${endpoint}`,
		requestOptions,
	);
	return response.json();
}

function filterTags(tags) {
	let words = [];
	if (tags.length !== 0) {
		words = Array.from(
			new Set(
				tags
					.split(" ")
					.filter(word => word.trim() !== "" && word.length >= 3)
					.map(word => word.slice(0, 20))
					.slice(0, 10),
			),
		);
	}

	return words;
}

function getRoleName(role) {
	switch (role) {
		case UserRole.OWNER:
			return "Właściciel";

		case UserRole.ADMIN:
			return "Admin";

		case UserRole.MODERATOR:
			return "Moderator";

		case UserRole.USER:
			return "Użytkownik";
	}
}

function getRoleColorClassName(role) {
	switch (role) {
		case UserRole.OWNER:
			return "has-text-danger";

		case UserRole.ADMIN:
			return "has-text-warning";

		case UserRole.MODERATOR:
			return "has-text-success";

		case UserRole.USER:
			return "";
	}
}

module.exports = {
	sendJsonRequest,
	filterTags,
	getRoleName,
	getRoleColorClassName,
};
