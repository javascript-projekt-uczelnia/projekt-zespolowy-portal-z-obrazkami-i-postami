import React from "react";
import ReactDOM from "react-dom/client";
import "bulma/css/bulma.min.css";
import App from "./components/App";
import reportWebVitals from "./reportWebVitals";

const rootElement = document.getElementById("root");
rootElement.style.height = "100%";

const root = ReactDOM.createRoot(rootElement);
root.render(<App />);

reportWebVitals();
