import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser, faPen } from "@fortawesome/free-solid-svg-icons";
import { formatDate } from "../../shared/shared";
import { getRoleColorClassName } from "../../Utils";

function AuthorInfo({
	author,
	createdAt,
	avatar,
	authorRole,
	isEdited,
	openEditModal,
}) {
	return (
		<div
			className="mb-3 columns m-0 p-0"
			style={{ height: isEdited ? "75px" : "50px", width: "100%" }}>
			<div
				className="column is-narrow p-0 m-0"
				style={{ display: "flex", alignItems: "center" }}>
				{avatar === null ? (
					<FontAwesomeIcon
						icon={faUser}
						style={{
							width: "50px",
							height: "50px",
						}}
					/>
				) : (
					<img
						src={avatar}
						alt="Avatar"
						style={{
							width: "50px",
							height: "50px",
							borderRadius: "50%",
							objectFit: "cover",
							objectPosition: "center",
						}}
					/>
				)}
			</div>

			<div className="column columns m-0 p-0 ml-3 is-flex is-flex-direction-column">
				<span className="column m-0 p-0">
					Dodano przez:{" "}
					<Link
						to={`/user/${author}`}
						className={getRoleColorClassName(authorRole)}>
						{author}
					</Link>
				</span>
				<span className="column m-0 p-0">{formatDate(createdAt)}</span>
				{isEdited && (
					<span className="column m-0 p-0 has-text-warning">
						Edytowano
					</span>
				)}
			</div>

			{openEditModal !== null && (
				<div className="column is-flex is-justify-content-flex-end is-align-items-center">
					<FontAwesomeIcon
						icon={faPen}
						style={{ cursor: "pointer" }}
						onClick={openEditModal}
					/>
				</div>
			)}
		</div>
	);
}

export default AuthorInfo;
