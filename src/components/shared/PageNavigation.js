import React from "react";

function PageNavigation({ page = 0, isPrevDisabled, isNextDisabled, onPageBtnClick }) {
	return (
		<div className="m-0 p-0 has-text-centered is-flex is-align-items-center is-justify-content-center">
			<button
				onClick={() => onPageBtnClick(-1)}
				disabled={isPrevDisabled}
				className="button is-primary is-medium mr-5">
				Poprzednia strona
			</button>
			<span className="is-size-3 has-text-white m-0 p-0">{page}</span>
			<button
				onClick={() => onPageBtnClick(1)}
				disabled={isNextDisabled}
				className="button is-primary is-medium ml-5">
				Następna strona
			</button>
		</div>
	);
}

export default PageNavigation;
