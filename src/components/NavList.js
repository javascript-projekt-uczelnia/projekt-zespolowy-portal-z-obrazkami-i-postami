import React from "react";
import { Link } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faImage,
	faComments,
	faRightToBracket,
	faArrowUpRightFromSquare,
	faUser,
	faRightFromBracket,
	faUsers,
} from "@fortawesome/free-solid-svg-icons";
import { useUser } from "../UserContext";
import { UserRole } from "../shared/shared";
import { sendJsonRequest } from "../Utils";

function NavLink({ route, icon, text }) {
	return (
		<ul className="menu-list">
			<li>
				<Link
					to={route}
					className="has-text-white is-inline-block button is-ghost">
					<span className="icon mr-3">
						<FontAwesomeIcon icon={icon} />
					</span>
					<span>{text}</span>
				</Link>
			</li>
		</ul>
	);
}

function NavList() {
	const { user, setUser } = useUser();
	const isLogged = user !== null && user !== undefined;
	const login = isLogged ? user.login : null;
	const isAdmin = isLogged && user.role !== UserRole.USER;

	const handleLogOut = async e => {
		e.preventDefault();

		await sendJsonRequest("logOut");
		setUser(null);
	};

	return (
		<aside className="menu border-right p-3">
			<p className="menu-label has-text-white mb-2 has-text-weight-bold">
				Przejdź do
			</p>

			<NavLink route={"/"} icon={faComments} text={"Forum"} />
			<NavLink route={"/gallery"} icon={faImage} text={"Galeria"} />

			{isAdmin && (
				<NavLink route={"/users"} icon={faUsers} text={"Użytkownicy"} />
			)}

			<p className="menu-label has-text-white mb-2 mt-2 has-text-weight-bold">
				Konto
			</p>
			{isLogged && login !== null ? (
				<>
					<NavLink
						route={`/user/${login}`}
						icon={faUser}
						text={login}
					/>

					<ul className="menu-list">
						<li>
							<button
								className="is-text has-text-white is-inline-block button"
								onClick={handleLogOut}>
								<span className="icon mr-3 fa-regular">
									<FontAwesomeIcon
										icon={faRightFromBracket}
									/>
								</span>
								<span>Wyloguj</span>
							</button>
						</li>
					</ul>
				</>
			) : (
				<>
					<NavLink
						route={"/login"}
						icon={faRightToBracket}
						text={"Logowanie"}
					/>
					<NavLink
						route={"/register"}
						icon={faArrowUpRightFromSquare}
						text={"Rejestracja"}
					/>
				</>
			)}
		</aside>
	);
}

export default NavList;
