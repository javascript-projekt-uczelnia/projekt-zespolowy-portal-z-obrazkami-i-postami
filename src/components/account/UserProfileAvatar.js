import React, { useState, useEffect } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";

const mb5 = 10 * 512 * 1024;

function UserProfileAvatar({ initialAvatar, isMyAccount }) {
	const [avatar, setAvatar] = useState(initialAvatar);

	useEffect(() => {
		setAvatar(initialAvatar);
	}, [initialAvatar]);

	const handleAvatarClick = () => {
		if (isMyAccount) {
			const input = document.createElement("input");
			input.type = "file";
			input.accept = "image/*";
			input.onchange = event => {
				const file = event.target.files[0];
				handleAvatarChange(file);
			};
			input.click();
		}
	};

	const handleAvatarChange = async file => {
		if (file) {
			const reader = new FileReader();
			const imageFileTypes = ["image/jpeg", "image/png", "image/gif"];

			if (file.size <= mb5 && imageFileTypes.includes(file.type)) {
				reader.onloadend = async () => {
					const formData = new FormData();
					formData.append("file", file);

					const response = await fetch(
						"http://localhost:5000/updateUserAvatar",
						{
							method: "POST",
							mode: "cors",
							credentials: "include",
							body: formData,
						},
					);

					if (response.ok) {
						setAvatar(reader.result);
					} else {
						alert("Wystąpił błąd podczas zmiany awataru.");
					}
				};

				reader.readAsDataURL(file);
			} else {
				alert(
					"Plik musi być mniejszy niż 5 MB i być obrazem (format JPEG, PNG lub GIF)!",
				);
			}
		}
	};

	return (
		<div
			style={{
				width: "100%",
				height: "100%",
				cursor: isMyAccount ? "pointer" : "auto",
				overflow: "hidden",
			}}
			onClick={handleAvatarClick}>
			{avatar === null ? (
				<FontAwesomeIcon
					icon={faUser}
					style={{
						width: "100%",
						height: "100%",
					}}
				/>
			) : (
				<img
					src={avatar}
					alt="Avatar"
					style={{
						width: "200px",
						height: "200px",
						borderRadius: "50%",
						objectFit: "cover",
						objectPosition: "center",
					}}
				/>
			)}
		</div>
	);
}

export default UserProfileAvatar;
