import React, { useState, useEffect, useRef, useCallback } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { sendJsonRequest, getRoleColorClassName } from "../../Utils";
import Post from "../forum/Post";
import PageNavigation from "../shared/PageNavigation";
import UserProfileDescription from "./UserProfileDescription";
import UserProfileAvatar from "./UserProfileAvatar";
import { useUser } from "../../UserContext";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
	faComments,
	faComment,
	faThumbsUp,
} from "@fortawesome/free-solid-svg-icons";

function UserProfile() {
	const { user } = useUser();
	const { login: loginParam } = useParams();
	const [profile, setProfile] = useState(null);

	const topRef = useRef(null);
	const [page, setPage] = useState(0);
	const [posts, setPosts] = useState([]);
	const [hasNextPage, setHasNextPage] = useState(0);
	const isMyAccount = user && user.login === loginParam;

	const navigate = useNavigate();

	const fetchProfile = useCallback(async () => {
		const response = await sendJsonRequest("getUserProfile", {
			login: loginParam,
		});

		setProfile(response);
	}, [loginParam]);

	useEffect(() => {
		fetchProfile();
	}, [fetchProfile, loginParam]);

	const fetchUserPosts = useCallback(async () => {
		const response = await sendJsonRequest("getUserPosts", {
			login: loginParam,
			page: page,
		});

		setPosts(response.posts);
		setHasNextPage(response.hasNextPage);

		if (topRef.current) {
			topRef.current.scrollIntoView({ behavior: "smooth" });
		}
	}, [page, loginParam]);

	useEffect(() => {
		fetchUserPosts();
	}, [fetchUserPosts]);

	const onPageChange = offset => {
		setPage(prevPage => Math.max(0, prevPage + offset));
	};

	const onTagClick = filtredTags => {
		navigate(`/forum/0/${filtredTags}`);
	};

	const onUpdateProfile = updatedProfile => {
		setProfile(updatedProfile);
	};

	return (
		<div
			className="columns is-flex is-flex-direction-column p-3 m-0"
			style={{ height: "100%", width: "960px" }}>
			{profile ? (
				<div
					className="columns is-flex is-flex-direction-column column is-narrow"
					style={{ height: "100%", width: "100%" }}>
					<div
						className="columns p-0 m-0 is-flex is-align-items-center"
						style={{ height: "200px" }}>
						<div
							className="column p-0 m-0 is-narrow"
							style={{ height: "100%", width: "200px" }}>
							<UserProfileAvatar
								initialAvatar={profile.avatar}
								isMyAccount={isMyAccount}
							/>
						</div>

						<div
							className="column p-0 m-0 is-flex is-justify-content-flex-start is-flex-direction-column is-align-items-center columns"
							style={{ height: "100%" }}>
							<span className="column is-narrow p-0">
								<span
									className={
										"is-size-3 " +
										getRoleColorClassName(profile.role)
									}>
									{loginParam}
								</span>
							</span>
							<div
								className="column is-narrow py-0 px-6 is-flex is-justify-content-space-between"
								style={{ width: "100%" }}>
								<div className="is-flex is-align-items-center">
									<FontAwesomeIcon
										icon={faComments}
										className="icon is-medium mr-3 has-text-info-40"
									/>
									<span className="is-size-4">
										{profile.postCount || 0}
									</span>
								</div>
								<div className="is-flex is-align-items-center">
									<FontAwesomeIcon
										icon={faComment}
										className="icon is-medium mr-3 has-text-info-40"
									/>
									<span className="is-size-4">
										{profile.commentCount || 0}
									</span>
								</div>
								<div className="is-flex is-align-items-center">
									<FontAwesomeIcon
										icon={faThumbsUp}
										className="icon is-medium mr-3 has-text-info-40"
									/>
									<span className="is-size-4">
										{profile.totalLikes || 0}
									</span>
								</div>
							</div>
							<UserProfileDescription
								profile={profile}
								loginParam={loginParam}
								user={user}
								isMyAccount={isMyAccount}
								onUpdateProfile={onUpdateProfile}
							/>
						</div>
					</div>

					<div
						className="column p-0 mt-3 is-flex is-flex-direction-column is-align-items-center pr-3"
						style={{
							height: "100%",
							width: "100%",
							overflow: "auto",
						}}>
						<div ref={topRef} />

						<div
							className="column p-0 m-0"
							style={{ width: "100%" }}>
							{posts.map((post, index) => (
								<Post
									key={post.id}
									id={post.id}
									title={post.title}
									author={post.author}
									content={post.content}
									createdAt={post.createdAt}
									initialLikesCount={post.likesCount}
									initialCommentsCount={post.commentsCount}
									avatar={post.avatar}
									tags={post.tags}
									onTagClick={onTagClick}
								/>
							))}
						</div>

						<div className="column is-narrow">
							<PageNavigation
								isPrevDisabled={page === 0}
								isNextDisabled={!hasNextPage}
								onPageBtnClick={onPageChange}
								page={page}
							/>
						</div>
					</div>
				</div>
			) : (
				<div
					className="is-flex is-align-items-center is-justify-content-center"
					style={{
						width: "100%",
						height: "100%",
					}}>
					<span className="is-size-1">
						Użytkownik {loginParam} nie istnieje!
					</span>
				</div>
			)}
		</div>
	);
}

export default UserProfile;
