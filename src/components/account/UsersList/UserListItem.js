import React, { useState } from "react";
import { Link } from "react-router-dom";
import BanModal from "./BanModal";
import UnbanModal from "./UnbanModal";
import ChangeRoleModal from "./ChangeRoleModal";

import { getRoleName, getRoleColorClassName } from "../../../Utils";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { formatDate } from "../../../shared/shared";

function UserListItem({ target, user }) {
	const [isBanModalOpen, setIsBanModalOpen] = useState(false);
	const [isUnbanModalOpen, setIsUnbanModalOpen] = useState(false);
	const [isChangeRoleModalOpen, setIsChangeRoleModalOpen] = useState(false);

	const canInfluence = user.role < target.role && user.login !== target.login;

	const handleChangeRange = async () => {
		setIsChangeRoleModalOpen(true);
	};

	const handleBan = () => {
		setIsBanModalOpen(true);
	};

	const handleUnban = () => {
		setIsUnbanModalOpen(true);
	};

	return (
		<div
			className="colums is-flex has-background-grey-darker px-3 py-0 mb-3 mt-0"
			style={{ width: "100%", height: "90px" }}>
			<div className="column columns m-0 p-0">
				<div
					className="p-0 m-0 is-narrow column"
					style={{ display: "flex", alignItems: "center" }}>
					{target.avatar === null ? (
						<FontAwesomeIcon
							icon={faUser}
							style={{
								width: "50px",
								height: "50px",
							}}
						/>
					) : (
						<img
							src={target.avatar}
							alt="Avatar"
							style={{
								width: "50px",
								height: "50px",
								borderRadius: "50%",
								objectFit: "cover",
								objectPosition: "center",
							}}
						/>
					)}
				</div>

				<div
					className="pl-3 is-narrow column is-flex is-flex-direction-column is-justify-content-center"
					style={{ height: "100%" }}>
					<span>
						<span className="has-text-weight-semibold">
							Nazwa:{" "}
						</span>
						<Link to={`/user/${target.login}`}>{target.login}</Link>
						<span
							className={
								getRoleColorClassName(target.role) + " ml-1"
							}>
							{getRoleName(target.role)}
						</span>
					</span>

					<span className="column m-0 p-0">
						<span className="has-text-weight-semibold">
							Utworzono:{" "}
						</span>
						<span>{formatDate(target.createdAt)}</span>
					</span>

					<span className="column m-0 p-0">
						<span className="has-text-weight-semibold">
							Email:{" "}
						</span>
						<span>{target.email}</span>
					</span>
				</div>
			</div>

			<div className="column is-flex is-align-items-center is-justify-content-space-around">
				<button
					className="button py-3 has-background-warning-30"
					style={{ width: "120px" }}
					onClick={() => handleChangeRange()}
					disabled={!canInfluence}>
					Zmień rangę
				</button>

				{!target.isBanned ? (
					<button
						className="button py-3 has-background-danger-30"
						style={{ width: "120px" }}
						onClick={() => handleBan()}
						disabled={!canInfluence}>
						Zbanuj
					</button>
				) : (
					<button
						className="button py-3 has-background-primary-20"
						style={{ width: "120px" }}
						onClick={() => handleUnban()}
						disabled={!canInfluence}>
						Odbanuj
					</button>
				)}

				{isChangeRoleModalOpen && (
					<ChangeRoleModal
						isOpen={isChangeRoleModalOpen}
						onClose={() => setIsChangeRoleModalOpen(false)}
						target={target}
						user={user}
					/>
				)}

				{!target.isBanned && (
					<BanModal
						isOpen={isBanModalOpen}
						onClose={() => setIsBanModalOpen(false)}
						target={target}
					/>
				)}

				{target.isBanned && (
					<UnbanModal
						isOpen={isUnbanModalOpen}
						onClose={() => setIsUnbanModalOpen(false)}
						target={target}
					/>
				)}
			</div>
		</div>
	);
}

export default UserListItem;
