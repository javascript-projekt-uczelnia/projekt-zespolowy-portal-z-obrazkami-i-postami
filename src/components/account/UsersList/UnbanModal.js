import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { sendJsonRequest } from "../../../Utils";
import { formatDate } from "../../../shared/shared";

function UnbanModal({ isOpen, onClose, target }) {
	const [banInfo, setBanInfo] = useState(null);

	useEffect(() => {
		const fetchBanInfo = async () => {
			const response = await sendJsonRequest("getBanInfo", {
				userId: target.id,
			});

			if (response.success) {
				setBanInfo(response.data);
			} else {
				alert(response.message);
				onClose();
			}
		};

		if (isOpen) {
			fetchBanInfo();
		}
	}, [isOpen, target.id, onClose]);

	const handleUnban = async () => {
		const response = await sendJsonRequest("unbanUser", {
			userId: target.id,
		});

		if (response.success) {
			alert("Użytkownik został odbanowany pomyślnie!");
			target.isBanned = false;
			onClose();
		} else {
			alert(response.message);
		}
	};

	return (
		<div className={`modal ${isOpen ? "is-active" : ""}`}>
			<div className="modal-background" onClick={onClose}></div>
			<div className="modal-card">
				<section className="modal-card-body p-5 m-0">
					<div
						className="m-0 p-0 is-flex is-justify-content-space-between is-align-items-center mb-3"
						style={{ width: "100%" }}>
						<span className="is-size-4">
							Użytkownik:{" "}
							<Link to={`/user/${target.login}`}>
								{target.login}
							</Link>
						</span>

						<button
							className="button"
							aria-label="close"
							onClick={onClose}>
							X
						</button>
					</div>

					{banInfo ? (
						<div>
							<p className="is-multiline">
								<strong>Powód:</strong> {banInfo.reason}
							</p>
							<p>
								<strong>Nadano:</strong>{" "}
								{formatDate(banInfo.dateStart)}
							</p>
							<p>
								<strong>Ważny do:</strong>{" "}
								{formatDate(banInfo.dateEnd)}
							</p>
							<p>
								<strong>Przyznano przez: </strong>{" "}
								<Link to={`/user/${banInfo.grantorLogin}`}>
									{banInfo.grantorLogin}
								</Link>
							</p>

							<div
								className="is-flex is-justify-content-flex-end"
								style={{ width: "100%" }}>
								<button
									className="button is-warning"
									onClick={handleUnban}>
									Odbanuj
								</button>
							</div>
						</div>
					) : (
						<div>Brak informacji o banie dla tego użytkownika.</div>
					)}
				</section>
			</div>
			<button
				className="modal-close is-large"
				aria-label="close"
				onClick={onClose}></button>
		</div>
	);
}

export default UnbanModal;
