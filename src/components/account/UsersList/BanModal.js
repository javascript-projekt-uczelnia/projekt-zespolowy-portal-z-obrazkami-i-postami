import React, { useState } from "react";
import { Link } from "react-router-dom";
import { sendJsonRequest } from "../../../Utils";

function BanModal({ isOpen, onClose, target }) {
	const [reason, setReason] = useState("");
	const [dateEnd, setEndDate] = useState("");

	const handleBan = async e => {
		e.preventDefault();

		const response = await sendJsonRequest("banUser", {
			userId: target.id,
			reason: reason,
			dateEnd: dateEnd,
		});

		if (response.success) {
			alert("Ban został dodany pomyślnie!");
			target.isBanned = true;
			onClose();
		} else {
			alert(response.message);
		}
	};

	return (
		<div className={`modal ${isOpen ? "is-active" : ""}`}>
			<div className="modal-background" onClick={onClose}></div>
			<div className="modal-card">
				<section className="modal-card-body p-5 m-0">
					<div
						className="m-0 p-0 is-flex is-justify-content-space-between is-align-items-center mb-3"
						style={{ width: "100%" }}>
						<span className="is-size-4">
							Użytkownik:{" "}
							<Link to={`/user/${target.login}`}>
								{target.login}
							</Link>
						</span>

						<button
							className="button"
							aria-label="close"
							onClick={onClose}>
							X
						</button>
					</div>

					<form onSubmit={handleBan}>
						<div className="field">
							<label className="label">Powód bana:</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={reason}
									onChange={e => setReason(e.target.value)}
									required
								/>
							</div>
						</div>
						<div className="field">
							<label className="label">
								Data zakończenia bana:
							</label>
							<div className="control">
								<input
									className="input"
									type="date"
									value={dateEnd}
									onChange={e => setEndDate(e.target.value)}
									required
								/>
							</div>
						</div>

						<div
							className="is-flex is-justify-content-flex-end"
							style={{ width: "100%" }}>
							<button type="submit" className="button is-warning">
								Zbanuj
							</button>
						</div>
					</form>
				</section>
			</div>
			<button
				className="modal-close is-large"
				aria-label="close"
				onClick={onClose}></button>
		</div>
	);
}

export default BanModal;
