import React, { useState } from "react";
import {
	sendJsonRequest,
	getRoleName,
	getRoleColorClassName,
} from "../../../Utils";
import { Link } from "react-router-dom";
import { UserRole } from "../../../shared/shared";

function ChangeRoleModal({ isOpen, onClose, user, target }) {
	const [newRole, setNewRole] = useState(target.role);

	const handleChangeRole = async () => {
		const response = await sendJsonRequest("changeUserRole", {
			userId: target.id,
			newRole: newRole,
		});

		if (response.success) {
			alert("Zmieniono rolę!");
			target.role = parseInt(newRole);
			onClose();
		} else {
			alert("Wystąpił błąd podczas zmiany roli.");
		}
	};

	return (
		<div className={`modal ${isOpen ? "is-active" : ""}`}>
			<div className="modal-background" onClick={onClose}></div>
			<div className="modal-card">
				<section className="modal-card-body p-5 m-0">
					<div
						className="m-0 p-0 is-flex is-justify-content-space-between is-align-items-center mb-3"
						style={{ width: "100%" }}>
						<span className="is-size-4">
							Użytkownik:{" "}
							<Link to={`/user/${target.login}`}>
								{target.login}
							</Link>
						</span>

						<button
							className="button"
							aria-label="close"
							onClick={onClose}>
							X
						</button>
					</div>

					<div className="control">
						<div className="select is-medium">
							<select
								value={newRole}
								onChange={e => setNewRole(e.target.value)}>
								{Array.from(
									{ length: UserRole.USER - user.role },
									(_, index) => {
										const roleValue = user.role + index + 1;
										return (
											<option
												key={roleValue}
												value={roleValue}
												className={getRoleColorClassName(
													roleValue,
												)}>
												{getRoleName(roleValue)}
											</option>
										);
									},
								)}
							</select>
						</div>
					</div>

					<div
						className="is-flex is-justify-content-flex-end"
						style={{ width: "100%" }}>
						<button
							className="button is-warning"
							onClick={handleChangeRole}>
							Zmień rolę
						</button>
					</div>
				</section>
			</div>
			<button
				className="modal-close is-large"
				aria-label="close"
				onClick={onClose}></button>
		</div>
	);
}

export default ChangeRoleModal;
