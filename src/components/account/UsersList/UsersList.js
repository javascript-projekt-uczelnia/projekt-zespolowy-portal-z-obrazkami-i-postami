import React, { useEffect, useCallback, useState } from "react";
import { useUser } from "../../../UserContext";
import { sendJsonRequest } from "../../../Utils";
import UserListItem from "./UserListItem";

function UsersList() {
	const { user } = useUser();
	const [users, setUsers] = useState([]);

	const fetchUsers = useCallback(async () => {
		if (user !== null) {
			const response = await sendJsonRequest("getUsers");
			setUsers(response);
		} else {
			setUsers([]);
		}
	}, [user]);

	useEffect(() => {
		fetchUsers();
	}, [fetchUsers]);

	return (
		<div
			className="has-background-black-ter p-3"
			style={{ height: "100%", width: "960px", overflowY: "auto" }}>
			{users.length !== 0 && user !== null ? (
				users.map(target => (
					<UserListItem key={target.id} target={target} user={user} />
				))
			) : (
				<p
					className="is-flex is-align-items-center is-justify-content-center is-size-1"
					style={{ width: "100%", height: "100%" }}>
					Brak użytkowników lub uprawnień.
				</p>
			)}
		</div>
	);
}

export default UsersList;
