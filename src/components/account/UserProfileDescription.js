	import React, { useState } from "react";
	import { sendJsonRequest } from "../../Utils";

	function UserProfileDescription({
		profile,
		loginParam,
		user,
		isMyAccount,
		onUpdateProfile,
	}) {
		const [descriptionEdit, setDescriptionEdit] = useState(profile.description);
		const [isEditingDescription, setIsEditingDescription] = useState(false);

		const handleDescriptionClick = () => {
			if (user && user.login === loginParam) {
				setDescriptionEdit(profile.description);
				setIsEditingDescription(true);
			}
		};

		const handleDescriptionChange = event => {
			setDescriptionEdit(event.target.value);
		};

		const handleConfirm = async () => {
			await sendJsonRequest("updateUserDescription", {
				description: descriptionEdit,
			});
			onUpdateProfile({ ...profile, description: descriptionEdit });
			setIsEditingDescription(false);
		};

		const handleReject = () => {
			setIsEditingDescription(false);
		};

		return (
			<>
				{isEditingDescription ? (
					<div
						className="px-4 py-0 my-3 column is-flex columns"
						style={{ width: "100%" }}>
						<div className="control p-0 m-0 column columns">
							<textarea
								autoFocus
								style={{
									width: "100%",
									backgroundColor: "rgba(0, 0, 0, 0)",
								}}
								value={descriptionEdit}
								onChange={handleDescriptionChange}
								className="is-flex is-size-5 has-text-light column"></textarea>
						</div>

						<div className="column is-narrow columns is-flex is-flex-direction-column m-0 mx-2 p-0">
							<button
								className="column button"
								onClick={handleConfirm}>
								Potwierdź
							</button>
							<button
								className="column button"
								onClick={handleReject}>
								Odrzuć
							</button>
						</div>
					</div>
				) : (
					<span
						className="px-4 py-0 column mt-3 is-size-5 is-flex"
						style={{
							overflowY: "auto",
							width: "100%",
							cursor: isMyAccount ? "pointer" : "auto",
						}}
						onClick={handleDescriptionClick}>
						{profile.description !== null &&
						profile.description.trim() !== "" ? (
							<>{profile.description}</>
						) : (
							<>
								{isMyAccount ? (
									<>Kliknij, by edytować opis.</>
								) : (
									<>Brak opisu.</>
								)}
							</>
						)}
					</span>
				)}
			</>
		);
	}

	export default UserProfileDescription;
