import React, { useState, useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { useUser } from "../../UserContext";

const { sendJsonRequest } = require("../../Utils");
function LogInForm() {
	const { user, setUser } = useUser();
	const navigate = useNavigate();
	const [formData, setFormData] = useState({
		login: "",
		password: "",
		errorMessage: "",
	});

	useEffect(() => {
		if (user !== null) {
			navigate("/");
		}
	}, [user, navigate]);

	const handleChange = e => {
		const { name, value } = e.target;
		setFormData(prevData => ({ ...prevData, [name]: value }));
	};

	const handleSubmit = async e => {
		e.preventDefault();

		try {
			const response = await sendJsonRequest("logIn", {
				login: formData.login,
				password: formData.password,
			});

			if (response.user !== null) {
				setFormData(prevData => ({
					...prevData,
					errorMessage: "",
				}));

				setUser(response.user);
				navigate("/");
			} else {
				setFormData(prevData => ({
					...prevData,
					errorMessage: response.message,
				}));
			}
		} catch (error) {}
	};

	return (
		<div className="is-flex is-flex-direction-column is-align-items-center p-0 m-0">
			<div>
				<Link to="/" className="has-text-grey-lighter">
					Wróć do strony głównej
				</Link>
			</div>

			<div className="has-background-black-ter box is-flex is-justify-content-center my-3 is-rounded">
				<form onSubmit={handleSubmit}>
					<div className="field">
						<div className="control">
							<input
								className="input is-medium"
								type="text"
								id="login"
								name="login"
								placeholder="Login"
								value={formData.login}
								onChange={handleChange}
								required
							/>
						</div>
					</div>

					<div className="field">
						<div className="control">
							<input
								className="input is-medium"
								type="password"
								id="password"
								name="password"
								placeholder="Hasło"
								value={formData.password}
								onChange={handleChange}
								required
							/>
						</div>
					</div>

					<div className="has-text-centered mb-3">
						{formData.errorMessage !== "" && (
							<p className="help is-danger is-size-6">
								{formData.errorMessage}
							</p>
						)}
					</div>

					<div className="field">
						<div className="control">
							<button
								className="button is-primary is-fullwidth is-medium"
								type="submit">
								Zaloguj się
							</button>
						</div>
					</div>
				</form>
			</div>

			<div>
				<span>Nie masz konta?&nbsp;</span>
				<Link to="/register" className="has-text-warning">
					<span>Zarejestruj się</span>
				</Link>
			</div>
		</div>
	);
}

export default LogInForm;
