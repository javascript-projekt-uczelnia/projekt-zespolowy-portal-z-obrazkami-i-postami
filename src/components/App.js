import React from "react";
import {
	BrowserRouter as Router,
	Route,
	Routes,
	Outlet,
} from "react-router-dom";
import NavList from "./NavList";
import Gallery from "./gallery/Gallery";
import Forum from "./forum/Forum";
import LogInForm from "./account/LogInForm";
import RegisterForm from "./account/RegisterForm";
import UserProfile from "./account/UserProfile";
import { UserProvider } from "../UserContext";
import UsersList from "./account/UsersList/UsersList";

function App() {
	return (
		<UserProvider>
			<Router>
				<div className="columns p-0 m-0" style={{ height: "100%" }}>
					<div className="column p-0 m-0 is-narrow has-background-black-ter">
						<NavList />
					</div>

					<div
						className="column p-0 m-0 is-flex is-justify-content-center is-align-items-center"
						style={{ height: "100%", width: "100%" }}>
						<Routes>
							<Route path="/" element={<Outlet />}>
								<Route index element={<Forum />} />
								<Route path="forum/" element={<Forum />} />
								<Route path="forum/:page" element={<Forum />} />
								<Route
									path="forum/:page/:tag"
									element={<Forum />}
								/>
							</Route>

							<Route path="/gallery" element={<Gallery />} />

							<Route path="/login" element={<LogInForm />} />

							<Route
								path="/register"
								element={<RegisterForm />}
							/>

							<Route
								path="/user/:login"
								element={<UserProfile />}
							/>

							<Route path="/users" element={<UsersList />} />
						</Routes>
					</div>
				</div>
			</Router>
		</UserProvider>
	);
}

export default App;
