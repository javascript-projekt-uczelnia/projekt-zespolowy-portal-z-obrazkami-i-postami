import React, { useState } from "react";

function AddPicture({ onAddPicture }) {
	const maxSize = 10 * 1024 * 1024;

	const [title, setTitle] = useState("");
	const [file, setFile] = useState("");
	const [previewImage, setPreviewImage] = useState(null);

	const handleTitleChange = event => {
		setTitle(event.target.value);
	};

	const handleImageChange = event => {
		const file = event.target.files[0];
		if (file && file.size <= maxSize) {
			const reader = new FileReader();
			const imageFileTypes = ["image/jpeg", "image/png", "image/gif"];

			if (file && imageFileTypes.includes(file.type)) {
				reader.onloadend = () => {
					setFile(file);
					setPreviewImage(reader.result);
				};

				reader.readAsDataURL(file);
			} else {
				setFile("");
				setPreviewImage(null);
			}
		} else {
			alert("Plik musi być mniejszy niż 10 MB!");
			setFile("");
			setPreviewImage(null);
		}
	};

	const handleSubmit = async () => {
		const formData = new FormData();
		formData.append("title", title);
		formData.append("file", file);

		const response = await fetch("http://localhost:5000/addPicture", {
			method: "POST",
			mode: "cors",
			credentials: "include",
			body: formData,
		});

		if (response.ok) {
			setTitle("");
			setFile("");
			setPreviewImage(null);
			onAddPicture();
		}
	};

	return (
		<div className="has-background-black-ter box is-rounded">
			<div className="field">
				<div className="control">
					<input
						type="text"
						id="title"
						placeholder="Wprowadź tytuł"
						className="input is-medium"
						value={title}
						onChange={handleTitleChange}
						required
					/>
				</div>
			</div>

			<div className="field">
				<div className="control">
					<input
						type="file"
						id="file"
						className="input is-medium"
						name="file"
						onChange={handleImageChange}
						accept=".jpg,.jpeg,.png,.gif"
						required
					/>
				</div>
			</div>

			{previewImage && (
				<div className="field is-flex is-justify-content-center">
					<img
						src={previewImage}
						alt="Podgląd obrazka"
						className="file"
						style={{ maxHeight: "960px", objectPosition: "center" }}
					/>
				</div>
			)}

			<div className="field mt-3">
				<div className="control has-text-right">
					<button
						onClick={handleSubmit}
						className="button is-primary is-fullwidth is-medium">
						Dodaj obrazek
					</button>
				</div>
			</div>
		</div>
	);
}

export default AddPicture;
