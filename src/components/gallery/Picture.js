import React from "react";
import AuthorInfo from "../shared/AuthorInfo";

function Picture({ title, file, author, createdAt, avatar, authorRole }) {
	return (
		<div className="has-background-black-ter box is-rounded my-3">
			<AuthorInfo
				author={author}
				createdAt={createdAt}
				avatar={avatar}
				authorRole={authorRole}
				openEditModal={null}
			/>

			<div className="pr-5">
				<h2 className="is-size-5 has-text-white">{title}</h2>
			</div>

			<img
				className="mt-3"
				src={file}
				alt="image"
				style={{
					width: "100%",
					height: "100%",
					objectFit: "cover",
					objectPosition: "center",
				}}
			/>
		</div>
	);
}

export default Picture;
