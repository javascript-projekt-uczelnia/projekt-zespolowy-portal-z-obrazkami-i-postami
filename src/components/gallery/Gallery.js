import React, { useState, useEffect, useRef, useCallback } from "react";
import { sendJsonRequest } from "../../Utils";
import PageNavigation from "../shared/PageNavigation";
import { useParams } from "react-router-dom";
import { useUser } from "../../UserContext";
import AddPicture from "./AddPicture";
import Picture from "./Picture";

function Gallery() {
	const { user } = useUser();
	const { page: pageParam } = useParams();
	const [page, setPage] = useState(pageParam ? parseInt(pageParam, 10) : 0);

	const [pictures, setPictures] = useState([]);
	const topRef = useRef(null);

	const [hasNextPage, setHasNextPage] = useState(0);

	const fetchPictures = useCallback(async () => {
		const response = await sendJsonRequest("getPictures", {
			page: page,
		});

		setPictures(response.pictures);
		setHasNextPage(response.hasNextPage);

		if (topRef.current) {
			topRef.current.scrollIntoView({ behavior: "smooth" });
		}
	}, [page, topRef]);

	useEffect(() => {
		fetchPictures();
	}, [fetchPictures]);

	const onPageChange = offset => {
		setPage(prevPage => Math.max(0, prevPage + offset));
	};

	const onAddPicture = () => {
		fetchPictures();
	};

	return (
		<div
			className="columns is-flex is-flex-direction-column is-align-items-center p-3"
			style={{ height: "100%", width: "100%", overflow: "auto" }}>
			<div ref={topRef} />

			{user !== null && page === 0 && (
				<div
					className="column is-narrow p-0 m-0"
					style={{ width: "960px" }}>
					<AddPicture onAddPicture={onAddPicture} />
				</div>
			)}

			<div className="column p-0 m-0" style={{ width: "960px" }}>
				{pictures.map((picture, index) => (
					<Picture
						key={picture.id}
						title={picture.title}
						file={picture.file}
						author={picture.author}
						createdAt={picture.createdAt}
						avatar={picture.avatar}
						authorRole={picture.authorRole}
					/>
				))}
			</div>

			<div className="column is-narrow">
				<PageNavigation
					isPrevDisabled={page === 0}
					isNextDisabled={!hasNextPage}
					onPageBtnClick={onPageChange}
					page={page}
				/>
			</div>
		</div>
	);
}

export default Gallery;
