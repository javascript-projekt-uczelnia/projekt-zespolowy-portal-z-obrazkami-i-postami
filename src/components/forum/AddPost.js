import React, { useState } from "react";
import { sendJsonRequest } from "../../Utils";
import { filterTags } from "../../Utils";

function AddPost({ onAddPost }) {
	const [title, setTitle] = useState("");
	const [content, setContent] = useState("");
	const [tags, setTags] = useState("");

	const handleSubmit = async event => {
		event.preventDefault();

		const message = await sendJsonRequest("addPost", {
			title: title,
			content: content,
			words: filterTags(tags),
		});

		alert(message);
		setTitle("");
		setContent("");
		setTags("");
		onAddPost();
	};

	return (
		<form
			className="has-background-black-ter box is-rounded"
			onSubmit={handleSubmit}>
			<div className="field">
				<div className="control">
					<input
						type="text"
						id="title"
						placeholder="Wprowadź tytuł"
						className="input is-medium"
						value={title}
						onChange={e => setTitle(e.target.value)}
						required
					/>
				</div>
			</div>
			<div className="field">
				<div className="control">
					<textarea
						id="content"
						placeholder="Wprowadź treść"
						className="textarea is-medium"
						value={content}
						onChange={e => setContent(e.target.value)}
						required
					/>
				</div>
			</div>
			<div className="control">
				<input
					type="text"
					id="tags"
					placeholder="Wprowadź tagi (opcjonalne, odziel spacją)"
					className="input is-medium"
					value={tags}
					onChange={e => setTags(e.target.value)}
				/>
			</div>
			<div className="field mt-3">
				<div className="control has-text-right">
					<button
						type="submit"
						className="button is-primary is-fullwidth is-medium">
						Dodaj post
					</button>
				</div>
			</div>
		</form>
	);
}

export default AddPost;