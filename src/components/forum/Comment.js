import React, { useState } from "react";
import { sendJsonRequest } from "../../Utils";
import EditCommentModal from "./EditCommentModal";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThumbsUp } from "@fortawesome/free-solid-svg-icons";

import { useUser } from "../../UserContext";
import AuthorInfo from "../shared/AuthorInfo";

function Comment({
	id,
	author,
	initialContent,
	createdAt,
	avatar,
	authorRole,
	initalEdited,
	initialLikesCount = 0,
}) {
	const { user } = useUser();
	const [likesCount, setLikesCount] = useState(initialLikesCount);
	const [isEditModalOpen, setIsEditModalOpen] = useState(false);
	const [content, setContent] = useState(initialContent);
	const [isEdited, setEdited] = useState(initalEdited);

	const onLikeClick = async event => {
		event.preventDefault();
		if (user === null) {
			return;
		}

		try {
			const response = await sendJsonRequest("addPostsCommentsLike", {
				id: id,
			});

			setLikesCount(response.likesCount);
		} catch (error) {
			console.error(error);
		}
	};

	const openEditModal = () => {
		setIsEditModalOpen(true);
	};

	const closeEditModal = () => {
		setIsEditModalOpen(false);
	};

	return (
		<div className="has-background-grey-darker py-3 box is-rounded has-text-white">
			<AuthorInfo
				author={author}
				createdAt={createdAt}
				avatar={avatar}
				authorRole={authorRole}
				isEdited={isEdited}
				openEditModal={
					user && (user.login === author || user.role < authorRole)
						? openEditModal
						: null
				}
			/>

			<p className="is-size-6 m-0 p-0">{content}</p>
			<div className="is-align-items-center is-justify-content-flex-end is-flex m-0 p-0">
				<span className="icon-text">
					<FontAwesomeIcon
						className="icon fa-regular has-text-info-40"
						icon={faThumbsUp}
						onClick={onLikeClick}
						style={{ cursor: "pointer" }}
					/>
				</span>

				<span className="ml-2">{likesCount}</span>
			</div>

			<EditCommentModal
				isOpen={isEditModalOpen}
				onClose={closeEditModal}
				commentId={id}
				initialContent={initialContent}
				onContentChange={setContent}
				onEditedChange={setEdited}
			/>
		</div>
	);
}

export default Comment;
