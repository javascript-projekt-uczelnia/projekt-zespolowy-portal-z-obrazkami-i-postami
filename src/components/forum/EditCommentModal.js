import React, { useState } from "react";
import { sendJsonRequest } from "../../Utils";

function EditCommentModal({
	isOpen,
	onClose,
	commentId,
	initialContent,
	onContentChange,
	onEditedChange,
}) {
	const [content, setContent] = useState(initialContent);

	const handleEditComment = async e => {
		e.preventDefault();
		const response = await sendJsonRequest("editPostComment", {
			commentId: commentId,
			content: content,
		});
		if (response.result) {
			alert("Komentarz został zaktualizowany pomyślnie!");
			onContentChange(content);
			onEditedChange(true);
			onClose();
		} else {
			alert(response.message);
		}
	};

	return (
		<div className={`modal ${isOpen ? "is-active" : ""}`}>
			<div className="modal-background" onClick={onClose}></div>
			<div className="modal-card">
				<section className="modal-card-body p-5 m-0">
					<div className="is-flex is-justify-content-space-between mb-3">
						<h2 className="title">Edytuj komentarz</h2>
						<button
							className="button p-3 m-0"
							aria-label="close"
							onClick={onClose}>
							X
						</button>
					</div>

					<form onSubmit={handleEditComment}>
						<div className="field">
							<div className="control">
								<textarea
									className="textarea"
									value={content}
									onChange={e => setContent(e.target.value)}
									required></textarea>
							</div>
						</div>

						<div className="is-flex is-justify-content-flex-end">
							<button type="submit" className="button is-primary">
								Zapisz zmiany
							</button>
						</div>
					</form>
				</section>
			</div>
			<button
				className="modal-close is-large"
				aria-label="close"
				onClick={onClose}></button>
		</div>
	);
}

export default EditCommentModal;
