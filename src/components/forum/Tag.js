import React from "react";

function Tag({ tagName, onTagClick }) {
	const handleClick = () => {
		onTagClick([tagName]);
	};

	return (
		<span
			className="has-text-link"
			style={{ cursor: "pointer" }}
			onClick={handleClick}>
			#{tagName}{" "}
		</span>
	);
}

export default Tag;
