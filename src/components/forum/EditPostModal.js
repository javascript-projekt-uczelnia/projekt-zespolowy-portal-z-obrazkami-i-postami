import React, { useState } from "react";
import { sendJsonRequest, filterTags } from "../../Utils";

function EditPostModal({
	isOpen,
	onClose,
	postId,
	initialTitle,
	initialContent,
	initialTags,
	onTitleChange,
	onContentChange,
	onTagsChange,
	onEditedChange,
}) {
	const [title, setTitle] = useState(initialTitle);
	const [content, setContent] = useState(initialContent);
	const [tags, setTags] = useState(initialTags.join(" "));

	const handleEditPost = async e => {
		e.preventDefault();
		const response = await sendJsonRequest("editPost", {
			postId: postId,
			title: title,
			content: content,
			words: filterTags(tags),
		});
		if (response.result) {
			alert("Post został zaktualizowany pomyślnie!");
			onTitleChange(title);
			onContentChange(content);
			onTagsChange(tags.split(" "));
			onEditedChange(true);
			onClose();
		} else {
			alert(response.message);
		}
	};

	return (
		<div className={`modal ${isOpen ? "is-active" : ""}`}>
			<div className="modal-background" onClick={onClose}></div>
			<div className="modal-card">
				<section className="modal-card-body p-5 m-0">
					<div className="is-flex is-justify-content-space-between">
						<h2 className="title">Edytuj post</h2>
						<button
							className="button p-3 m-0"
							aria-label="close"
							onClick={onClose}>
							X
						</button>
					</div>

					<form onSubmit={handleEditPost}>
						<div className="field">
							<label className="label">Tytuł:</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={title}
									onChange={e => setTitle(e.target.value)}
									required
								/>
							</div>
						</div>
						<div className="field">
							<label className="label">Treść:</label>
							<div className="control">
								<textarea
									className="textarea"
									value={content}
									onChange={e => setContent(e.target.value)}
									required></textarea>
							</div>
						</div>
						<div className="field">
							<label className="label">Tagi:</label>
							<div className="control">
								<input
									className="input"
									type="text"
									value={tags}
									onChange={e => setTags(e.target.value)}
									required
								/>
							</div>
						</div>

						<div className="is-flex is-justify-content-flex-end">
							<button type="submit" className="button is-primary">
								Zapisz zmiany
							</button>
						</div>
					</form>
				</section>
			</div>
			<button
				className="modal-close is-large"
				aria-label="close"
				onClick={onClose}></button>
		</div>
	);
}

export default EditPostModal;
