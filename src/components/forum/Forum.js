import React, { useState, useEffect, useRef, useCallback } from "react";
import { sendJsonRequest } from "../../Utils";
import Post from "./Post";
import PageNavigation from "../shared/PageNavigation";
import AddPost from "./AddPost";
import SearchPost from "./SearchPost";
import { useParams } from "react-router-dom";
import { useUser } from "../../UserContext";

function Forum() {
	const { user } = useUser();
	const { page: pageParam, tag: tagParam } = useParams();
	const [tags, setTags] = useState(tagParam ? [tagParam] : []);
	const [page, setPage] = useState(pageParam ? parseInt(pageParam, 10) : 0);

	const [posts, setPosts] = useState([]);
	const topRef = useRef(null);

	const [hasNextPage, setHasNextPage] = useState(0);

	const fetchPosts = useCallback(async () => {
		const response = await sendJsonRequest("getPosts", {
			page: page,
			tags: tags,
		});

		setPosts(response.posts);
		setHasNextPage(response.hasNextPage);

		if (topRef.current) {
			topRef.current.scrollIntoView({ behavior: "smooth" });
		}
	}, [page, tags, topRef]);

	useEffect(() => {
		fetchPosts();
	}, [fetchPosts]);

	const onPageChange = offset => {
		setPage(prevPage => Math.max(0, prevPage + offset));
	};

	const onAddPost = () => {
		fetchPosts();
	};

	const onSearch = filtredTags => {
		setTags(filtredTags);
		setPage(0);
	};

	const onTagClick = filtredTags => {
		setTags(filtredTags);
		setPage(0);
	};

	return (
		<div
			className="columns is-flex is-flex-direction-column is-align-items-center p-3"
			style={{ height: "100%", width: "100%", overflow: "auto" }}>
			<div ref={topRef} />

			<div
				style={{ width: "100%" }}
				className="column is-narrow p-0 m-0 is-flex is-justify-content-flex-end">
				<SearchPost onSearch={onSearch} />
			</div>

			{user !== null && page === 0 && (
				<div
					className="column is-narrow p-0 m-0"
					style={{ width: "960px" }}>
					<AddPost onAddPost={onAddPost} />
				</div>
			)}

			<div className="column p-0 m-0" style={{ width: "960px" }}>
				{posts.map((post, index) => (
					<Post
						key={post.id}
						id={post.id}
						initalTitle={post.title}
						author={post.author}
						initalContent={post.content}
						createdAt={post.createdAt}
						initialLikesCount={post.likesCount}
						initialCommentsCount={post.commentsCount}
						avatar={post.avatar}
						authorRole={post.authorRole}
						initalEdited={post.isEdited}
						initalTags={post.tags}
						onTagClick={onTagClick}
					/>
				))}
			</div>

			<div className="column is-narrow">
				<PageNavigation
					isPrevDisabled={page === 0}
					isNextDisabled={!hasNextPage}
					onPageBtnClick={onPageChange}
					page={page}
				/>
			</div>
		</div>
	);
}

export default Forum;
