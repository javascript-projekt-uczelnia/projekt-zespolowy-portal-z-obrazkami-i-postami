import React, { useState } from "react";
import { sendJsonRequest } from "../../Utils";

function AddComment({ postId, onAddComment }) {
	const [content, setContent] = useState("");

	const handleSubmit = async event => {
		event.preventDefault();

		const message = await sendJsonRequest("addPostsComment", {
			postId: postId,
			content: content,
		});

		alert(message);
		setContent("");
		onAddComment();
	};

	return (
		<form className="my-3" onSubmit={handleSubmit}>
			<div className="field">
				<div className="control">
					<textarea
						id="content"
						className="textarea"
						value={content}
						onChange={e => setContent(e.target.value)}
						required
					/>
				</div>
			</div>
			<div className="field">
				<div className="control has-text-right">
					<button type="submit" className="button is-primary">
						Dodaj komentarz
					</button>
				</div>
			</div>
		</form>
	);
}

export default AddComment;
