import React, { useState } from "react";
import { filterTags } from "../../Utils";

function SearchPost({ onSearch }) {
	const [tags, setTags] = useState("");

	const handleSearch = event => {
		event.preventDefault();
		onSearch(filterTags(tags));
	};

	const handleClear = event => {
		event.preventDefault();
		setTags("");
		onSearch([]);
	};

	const handleChange = event => {
		setTags(event.target.value);
	};

	return (
		<div className="is-flex">
			<div className="field">
				<div className="control">
					<input
						type="text"
						placeholder="Wprowadź tagi (po spacji)"
						className="input is-medium m-0"
						value={tags}
						onChange={handleChange}
					/>
				</div>
			</div>
			<div className="field ml-3">
				<div className="control">
					<button
						className="button is-primary is-fullwidth is-medium"
						onClick={handleSearch}>
						Wyszukaj
					</button>
				</div>
			</div>

			<div className="field ml-3">
				<div className="control">
					<button
						className="button is-primary is-fullwidth is-medium"
						onClick={handleClear}>
						Wyczyść
					</button>
				</div>
			</div>
		</div>
	);
}

export default SearchPost;
