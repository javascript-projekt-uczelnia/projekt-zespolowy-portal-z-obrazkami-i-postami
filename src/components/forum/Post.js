import React, { useState, useEffect, useCallback } from "react";
import { sendJsonRequest } from "../../Utils";
import Comment from "./Comment";
import AddComment from "./AddComment";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faThumbsUp, faComment } from "@fortawesome/free-solid-svg-icons";
import Tag from "./Tag";
import { useUser } from "../../UserContext";
import AuthorInfo from "../shared/AuthorInfo";
import EditPostModal from "./EditPostModal";

function Post({
	id,
	initalTitle,
	author,
	initalContent,
	createdAt,
	initialLikesCount = 0,
	initialCommentsCount = 0,
	avatar,
	authorRole,
	initalEdited,
	initalTags = [],
	onTagClick,
}) {
	const { user } = useUser();
	const [title, setTitle] = useState(initalTitle);
	const [content, setContent] = useState(initalContent);
	const [tags, setTags] = useState(initalTags);
	const [isEdited, setEdited] = useState(initalEdited);

	const [likesCount, setLikesCount] = useState(initialLikesCount);
	const [commentsCount, setCommentsCount] = useState(initialCommentsCount);
	const [comments, setComments] = useState([]);
	const [commentsVisible, setCommentsVisible] = useState(false);
	const [isEditModalOpen, setIsEditModalOpen] = useState(false);

	const fetchComments = useCallback(async () => {
		const comments = await sendJsonRequest("getPostsComments", { id });
		setComments(comments);
		setCommentsCount(comments.length);
	}, [id]);

	const onLikeClick = async event => {
		event.preventDefault();
		if (user === null) {
			return;
		}

		try {
			const response = await sendJsonRequest("addPostsLike", {
				id: id,
			});

			setLikesCount(response.likesCount);
		} catch (error) {
			console.error(error);
		}
	};

	const onCommentClick = event => {
		event.preventDefault();
		setCommentsVisible(commentsVisible => !commentsVisible);
	};

	useEffect(() => {
		if (commentsVisible) {
			fetchComments();
		} else {
			setComments([]);
		}
	}, [commentsVisible, fetchComments]);

	const onAddComment = () => {
		fetchComments();
	};

	const openEditModal = () => {
		setIsEditModalOpen(true);
	};

	const closeEditModal = () => {
		setIsEditModalOpen(false);
	};

	return (
		<div className="has-background-black-ter box is-rounded my-3">
			<AuthorInfo
				author={author}
				createdAt={createdAt}
				avatar={avatar}
				authorRole={authorRole}
				isEdited={isEdited}
				openEditModal={
					user && (user.login === author || user.role < authorRole)
						? openEditModal
						: null
				}
			/>

			<div
				className="pr-5"
				style={{ maxHeight: "480px", overflowY: "auto" }}>
				<h2 className="is-size-5 has-text-white">{title}</h2>
				<p className="is-size-5">{content}</p>
			</div>

			{commentsVisible && (
				<>
					{user !== null && (
						<AddComment
							postId={id}
							onAddComment={onAddComment}></AddComment>
					)}

					{comments.length > 0 && (
						<div
							className="my-3 pr-3"
							style={{ maxHeight: "480px", overflowY: "auto" }}>
							{comments.map((comment, index) => (
								<Comment
									key={comment.id}
									id={comment.id}
									initialContent={comment.content}
									author={comment.author}
									createdAt={comment.createdAt}
									avatar={comment.avatar}
									authorRole={comment.authorRole}
									initalEdited={comment.isEdited}
									initialLikesCount={comment.likesCount}
								/>
							))}
						</div>
					)}
				</>
			)}

			<div className="columns mt-1">
				<div className="column mr-3">
					{tags.length !== 0 &&
						tags.map((tag, index) => (
							<Tag
								key={index}
								tagName={tag}
								onTagClick={onTagClick}
							/>
						))}
				</div>

				<div className="column is-narrow is-flex is-align-items-end ml-3">
					<span className="icon-text">
						<FontAwesomeIcon
							className="icon fa-regular has-text-info-40"
							icon={faThumbsUp}
							onClick={onLikeClick}
							style={{ cursor: "pointer" }}
						/>
						<span className="ml-1">{likesCount}</span>
					</span>

					<span className="icon-text ml-5">
						<FontAwesomeIcon
							className="icon fa-regular has-text-info-40"
							icon={faComment}
							onClick={onCommentClick}
							style={{ cursor: "pointer" }}
						/>
						<span className="ml-1">{commentsCount}</span>
					</span>
				</div>
			</div>

			<EditPostModal
				isOpen={isEditModalOpen}
				onClose={closeEditModal}
				postId={id}
				initialTitle={title}
				initialContent={content}
				initialTags={tags}
				onTitleChange={setTitle}
				onContentChange={setContent}
				onTagsChange={setTags}
				onEditedChange={setEdited}
			/>
		</div>
	);
}

export default Post;
