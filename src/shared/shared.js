const UserRole = {
	OWNER: 0,
	ADMIN: 1,
	MODERATOR: 2,
	USER: 3,
};

const formatDate = date => {
	const options = {
		year: "numeric",
		month: "numeric",
		day: "numeric",
		hour: "numeric",
		minute: "numeric",
		timeZone: "UTC",
	};

	return new Date(date).toLocaleString("pl-PL", options);
};

module.exports = { UserRole, formatDate };
