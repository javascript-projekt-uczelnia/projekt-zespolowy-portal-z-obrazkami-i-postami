const express = require("express");
const PostsService = require("../../services/PostsService");
const router = express.Router();

router.post("/", async (req, res) => {
	const { user } = req.session;
	if (user !== null) {
		try {
			const likeCount = await PostsService.toggleLike(user.id, req.body.id);

			res.status(200).json(likeCount);
		} catch (error) {
			res.status(500).json({ message: "Wystąpił błąd!" });
		}
	} else {
		res.status(500).json({ message: "Wystąpił błąd!" });
	}
});

module.exports = router;
