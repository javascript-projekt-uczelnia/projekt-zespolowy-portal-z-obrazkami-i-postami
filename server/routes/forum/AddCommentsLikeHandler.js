const express = require("express");
const PostsCommentsService = require("../../services/PostsCommentsService");
const router = express.Router();

router.post("/", async (req, res) => {
	const { user } = req.session;
	if (user !== null) {
		try {
			const likeCount = await PostsCommentsService.toggleLike(
				user.id,
				req.body.id,
			);

			res.status(200).json(likeCount);
		} catch (error) {
			res.status(500).json({ message: "Wystąpił błąd!" });
		}
	} else {
		res.status(500).json({ message: "Wystąpił błąd!" });
	}
});

module.exports = router;
