const express = require("express");
const router = express.Router();
const pool = require("../../database");
const PostsCommentsService = require("../../services/PostsCommentsService");

router.post("/", async (req, res) => {
	const { user } = req.session;
	if (user !== null) {
		try {
			await PostsCommentsService.addComment(req.body.postId, req.body.content, user.id);
			res.status(200).json("Dodano komentarz!");
		} catch (error) {
			res.status(500).json("Wystąpił błąd!");
		}
	} else {
		res.status(500).json("Wystąpił błąd!");
	}
});

module.exports = router;
