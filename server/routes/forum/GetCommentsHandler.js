const express = require("express");
const PostsCommentsService = require("../../services/PostsCommentsService");
const router = express.Router();

router.post("/", async (req, res) => {
	const data = await PostsCommentsService.getComments(req.body.id);
	res.json(data);
});

module.exports = router;
