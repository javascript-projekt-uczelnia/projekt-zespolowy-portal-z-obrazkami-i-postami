const express = require("express");
const PostsService = require("../../services/PostsService");
const router = express.Router();

router.post("/", async (req, res) => {
	const data = await PostsService.getPosts(req.body.page, req.body.tags);
	res.json(data);
});

module.exports = router;
