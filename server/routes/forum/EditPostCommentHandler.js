const express = require("express");
const router = express.Router();
const pool = require("../../database");
const UserService = require("../../services/UserService");
const PostsCommentsService = require("../../services/PostsCommentsService");

router.post("/", async (req, res) => {
	try {
		const { user } = req.session;
		const { commentId, content } = req.body;
		const commentOwner =
			await PostsCommentsService.getCommentOwner(commentId);

		if (commentOwner.id !== user.id) {
			const editorRole = await UserService.getRole(user.id);

			if (editorRole < commentOwner.role) {
				await PostsCommentsService.editComment(commentId, content);
				res.status(200).json({ result: true });
			} else {
				res.status(500).json({
					result: false,
					message: "Brak uprawnień!",
				});
				return;
			}
		} else {
			await PostsCommentsService.editComment(commentId, content);
			res.status(200).json({ result: true });
		}
	} catch (error) {
		res.status(500).json({
			result: false,
			message: "Wystąpił błąd!",
		});
	}
});

module.exports = router;
