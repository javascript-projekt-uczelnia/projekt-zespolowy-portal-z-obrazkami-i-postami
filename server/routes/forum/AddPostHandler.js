const express = require("express");
const router = express.Router();
const pool = require("../../database");
const PostsService = require("../../services/PostsService");

router.post("/", async (req, res) => {
	const { user } = req.session;
	if (user !== null) {
		try {
			await PostsService.addPost(
				req.body.title,
				req.body.content,
				req.body.words,
				user.id,
			);

			res.status(200).json("Dodano post!");
		} catch (error) {
			res.status(500).json("Wystąpił błąd!");
		}
	} else {
		res.status(500).json("Wystąpił błąd!");
	}
});

module.exports = router;
