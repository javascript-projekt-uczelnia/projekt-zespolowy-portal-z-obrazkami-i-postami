const express = require("express");
const router = express.Router();
const pool = require("../../database");
const PostsService = require("../../services/PostsService");
const UserService = require("../../services/UserService");

router.post("/", async (req, res) => {
	try {
		const { user } = req.session;
		const { postId, title, content, words } = req.body;
		const postOwner = await PostsService.getPostsOwner(postId);

		if (postOwner.id !== user.id) {
			const editorRole = await UserService.getRole(user.id);

			if (editorRole < postOwner.role) {
				await PostsService.editPost(postId, title, content, words);
				res.status(200).json({ result: true });
			} else {
				res.status(500).json({
					result: false,
					message: "Brak uprawnień!",
				});
				return;
			}
		} else {
			await PostsService.editPost(postId, title, content, words);
			res.status(200).json({ result: true });
		}
	} catch (error) {
		res.status(500).json({
			result: false,
			message: "Wystąpił błąd!",
		});
	}
});

module.exports = router;
