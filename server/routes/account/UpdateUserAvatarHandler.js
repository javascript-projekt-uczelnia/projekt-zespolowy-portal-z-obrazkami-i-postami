const express = require("express");
const router = express.Router();
const multer = require("multer");
const UserService = require("../../services/UserService");

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

router.post("/", upload.single("file"), async (req, res) => {
	const { user } = req.session;

	if (!user) {
		res.status(401).json();
	} else {
		try {
			const data = await UserService.updateAvatar(
				user.id,
				req.file.buffer,
			);
			res.status(200).json();
		} catch (error) {
			res.status(500).json();
		}
	}
});

module.exports = router;
