const express = require("express");
const router = express.Router();
const UserService = require("../../services/UserService");

router.post("/", async (req, res) => {
	const { login, password } = req.body;

	try {
		const data = await UserService.logIn(login, password);

		req.session.user = {
			id: data.id,
			login: login,
		};

		const user = await UserService.getSessionUser(req.session.user);
		res.status(200).json({ user });
	} catch (error) {
		res.status(401).json({
			user: null,
			message: error.message || "Wystąpił błąd!",
		});
	}
});

module.exports = router;
