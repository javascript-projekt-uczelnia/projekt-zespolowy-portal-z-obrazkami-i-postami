const express = require("express");
const router = express.Router();
const UserService = require("../../services/UserService");

router.post("/", async (req, res) => {
	const { user } = req.session;
	if (user === null) {
		res.status(401).json(false);
	} else {
		const data = await UserService.updateDescription(
			user.id,
			req.body.description,
		);

		res.status(200).json(data);
	}
});

module.exports = router;
