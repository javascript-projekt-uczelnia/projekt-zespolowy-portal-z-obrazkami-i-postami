const express = require("express");
const router = express.Router();

router.post("/", (req, res) => {
	const { user } = req.session;
	if (user) {
		req.session.user = null;
	}

	res.status(200).json({ message: "Wylogowano pomyślnie." });
});

module.exports = router;
