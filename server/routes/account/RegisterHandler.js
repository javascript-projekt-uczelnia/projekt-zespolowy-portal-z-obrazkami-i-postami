const express = require("express");
const router = express.Router();
const UserService = require("../../services/UserService");

router.post("/", async (req, res) => {
	const { login, email, password } = req.body;
	try {
		const data = await UserService.registerIn(login, email, password);
		if (data.result === true) {
			req.session.user = {
				id: data.id,
				login: login,
			};

			res.status(200).json({
				result: true,
				user: await UserService.getSessionUser(req.session.user),
			});
		} else {
			res.status(400).json({ result: false, message: message });
		}
	} catch (error) {
		res.status(500).json({ result: false, message: "Wystąpił błąd!" });
	}
});

module.exports = router;
