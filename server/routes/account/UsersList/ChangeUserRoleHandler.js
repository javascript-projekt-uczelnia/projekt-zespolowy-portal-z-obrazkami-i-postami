const express = require("express");
const router = express.Router();
const UserService = require("../../../services/UserService");

router.post("/", async (req, res) => {
	try {
		const grantor = req.session.user;
		const { userId, newRole } = req.body;

		const grantorRole = await UserService.getRole(grantor.id);
		const targetRole = await UserService.getRole(userId);

		if (grantorRole < newRole + 1 && grantorRole < targetRole + 1) {
			const result = await UserService.changeRole(userId, newRole);
			res.status(200).json({ success: true });
		} else {
			res.status(401).json({
				success: false,
				message: "Brak uprawnień.",
			});
		}
	} catch (error) {
		res.status(500).json({ success: false, message: error.message });
	}
});

module.exports = router;
