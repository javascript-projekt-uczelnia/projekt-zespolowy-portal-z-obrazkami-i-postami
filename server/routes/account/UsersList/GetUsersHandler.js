const express = require("express");
const router = express.Router();
const UserService = require("../../../services/UserService");
const { UserRole } = require("../../../../src/shared/shared");

router.post("/", async (req, res) => {
	const { user } = req.session;
	if (!user) {
		res.status(401).json([]);
		return;
	}

	if ((await UserService.getRole(user.id)) === UserRole.USER) {
		res.status(401).json([]);
		return;
	}

	const data = await UserService.getUsers();
	res.status(200).json(data);
});

module.exports = router;
