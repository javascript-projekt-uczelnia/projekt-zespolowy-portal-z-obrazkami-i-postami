const express = require("express");
const router = express.Router();
const UserService = require("../../../services/UserService");

router.post("/", async (req, res) => {
	try {
		const grantor = req.session.user;
		const userId = req.body.userId;

		if (grantor.id !== userId) {
			const result = await UserService.getBanInfo(userId);
			res.status(200).json({ success: true, data: result });
		} else {
			res.status(401).json({
				success: false,
				message: "Brak uprawnień.",
			});
		}
	} catch (error) {
		res.status(500).json({ success: false, message: error.message });
	}
});

module.exports = router;
