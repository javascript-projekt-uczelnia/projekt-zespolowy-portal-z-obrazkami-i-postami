const express = require("express");
const router = express.Router();
const UserService = require("../../services/UserService");

router.post("/", async (req, res) => {
	try {
		const data = await UserService.getProfile(req.body.login);
		res.status(200).json(data);
	} catch (error) {
		res.status(401).json(null);
	}
});

module.exports = router;
