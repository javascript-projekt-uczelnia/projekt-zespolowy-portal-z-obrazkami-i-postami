const express = require("express");
const UserService = require("../../services/UserService");
const router = express.Router();

router.post("/", async (req, res) => {
	res.json(await UserService.getSessionUser(req.session.user));
});

module.exports = router;
