const express = require("express");
const PostsService = require("../../services/PostsService");
const router = express.Router();

router.post("/", async (req, res) => {
	const data = await PostsService.getPosts(
		req.body.page,
		null,
		req.body.login,
	);
	res.json(data);
});

module.exports = router;
