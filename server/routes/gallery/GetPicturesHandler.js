const express = require("express");
const GalleryService = require("../../services/GalleryService");
const router = express.Router();

router.post("/", async (req, res) => {
	const data = await GalleryService.getPictures(req.body.page);
	res.json(data);
});

module.exports = router;
