const express = require("express");
const router = express.Router();
const multer = require("multer");
const GalleryService = require("../../services/GalleryService");

const storage = multer.memoryStorage();
const upload = multer({ storage: storage });

router.post("/", upload.single("file"), async (req, res) => {
	const { user } = req.session;
	if (user !== null) {
		try {
			await GalleryService.addPicture(
				req.body.title,
				req.file.buffer,
				user.id,
			);

			res.status(200).json({ message: "Dodano obrazek!" });
		} catch (error) {
			res.status(500).json({ message: "Wystąpił błąd!" });
		}
	} else {
		res.status(500).json({ message: "Wystąpił błąd!" });
	}
});

module.exports = router;
