const express = require("express");
const session = require("express-session");
const cors = require("cors");
const moment = require("moment-timezone");
const app = express();
const GalleryService = require("./services/GalleryService");

app.use(express.json());

app.use(
	session({
		secret: "someSecretKey",
		resave: false,
		saveUninitialized: false,
		cookie: {
			httpOnly: true,
			secure: false,
		},
	}),
);

app.use(
	cors({
		origin: "http://localhost:3000",
		credentials: true,
	}),
);

moment.locale("pl");
moment.tz.setDefault("Europe/Warsaw");

// Routing:
app.use("/checkUserStatus", require("./routes/account/CheckUserStatusHandler"));
app.use("/getUserProfile", require("./routes/account/GetUserProfileHandler"));
app.use("/getUserPosts", require("./routes/account/GetUserPostsHandler"));

app.use("/register", require("./routes/account/RegisterHandler"));
app.use("/logIn", require("./routes/account/LogInHandler"));
app.use("/logOut", require("./routes/account/LogOutHandler"));

app.use(
	"/updateUserDescription",
	require("./routes/account/UpdateUserDescriptionHandler"),
);

app.use(
	"/updateUserAvatar",
	require("./routes/account/UpdateUserAvatarHandler"),
);

app.use("/getUsers", require("./routes/account/UsersList/GetUsersHandler"));
app.use("/getBanInfo", require("./routes/account/UsersList/GetBanInfoHandler"));
app.use("/banUser", require("./routes/account/UsersList/BanUserHandler"));
app.use("/unbanUser", require("./routes/account/UsersList/UnbanUserHandler"));
app.use(
	"/changeUserRole",
	require("./routes/account/UsersList/ChangeUserRoleHandler"),
);

app.use("/addPicture", require("./routes/gallery/AddPictureHandler"));
app.use("/getPictures", require("./routes/gallery/GetPicturesHandler"));

app.use("/getPosts", require("./routes/forum/GetPostsHandler"));
app.use("/addPost", require("./routes/forum/AddPostHandler"));
app.use("/addPostsLike", require("./routes/forum/AddPostsLikeHandler"));
app.use("/editPost", require("./routes/forum/EditPostHandler"));

app.use("/getPostsComments", require("./routes/forum/GetCommentsHandler"));
app.use("/addPostsComment", require("./routes/forum/AddCommentHandler"));
app.use(
	"/addPostsCommentsLike",
	require("./routes/forum/AddCommentsLikeHandler"),
);
app.use("/editPostComment", require("./routes/forum/EditPostCommentHandler"));

const PORT = process.env.PORT || 5000;

app.listen(PORT, async () => {});
