class Utils {
	static convertImageToLink(imageBuffer) {
		if (imageBuffer) {
			const base64Image = imageBuffer.toString("base64");
			return `data:image/jpeg;base64,${base64Image}`;
		} else {
			return null;
		}
	}
}

module.exports = Utils;
