const pool = require("../database");
const moment = require("moment-timezone");
const UserService = require("./UserService");
const Utils = require("../Utils");

class PostsCommentsService {
	static async getLikesCount(commentId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const getLikeCountSql =
					"SELECT COUNT(user_id) AS likeCount FROM posts_comments_likes WHERE comment_id = ?";
				connection.query(
					getLikeCountSql,
					[commentId],
					(err, results) => {
						connection.release();
						if (err) {
							reject(err);
						} else {
							resolve(results[0].likeCount);
						}
					},
				);
			});
		});
	}

	static getComments(postId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const sql =
					"SELECT posts_comments.id, posts_comments.content, posts_comments.isEdited, users.id AS authorId, users.login AS author, posts_comments.created_at, users.avatar " +
					"FROM posts_comments " +
					"INNER JOIN users ON posts_comments.user_id = users.id " +
					"WHERE posts_comments.post_id = ? " +
					"ORDER BY posts_comments.created_at ASC";

				connection.query(sql, [postId], async (err, results) => {
					if (err) {
						connection.release();
						reject(err);
						return;
					}

					const commentsPromises = results.map(async result => {
						const comment = {
							id: result.id,
							content: result.content,
							author: result.author,
							createdAt: result.created_at,
							avatar: Utils.convertImageToLink(result.avatar),
							authorRole: await UserService.getRole(
								result.authorId,
							),
							isEdited: result.isEdited === 1,
							likesCount: 0,
						};

						const likesCountPromise =
							PostsCommentsService.getLikesCount(result.id);
						const commentWithLikesCountPromise = likesCountPromise
							.then(likesCount => {
								comment.likesCount = likesCount;
								return comment;
							})
							.catch(error => {
								console.error(
									"Error fetching likes count:",
									error,
								);
								return null;
							});

						return commentWithLikesCountPromise;
					});

					Promise.all(commentsPromises)
						.then(commentsArray => {
							connection.release();
							const filteredComments = commentsArray.filter(
								comment => comment !== null,
							);
							resolve(filteredComments);
						})
						.catch(error => {
							connection.release();
							reject(error);
						});
				});
			});
		});
	}

	static async addComment(postId, content, userId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const sql =
					"INSERT INTO `posts_comments`(`post_id`, `content`, `user_id`, `created_at`) VALUES (?, ?, ?, ?)";

				connection.query(
					sql,
					[postId, content, userId, moment().toDate()],
					(err, result) => {
						if (err) {
							connection.release();
							reject(err);
							return;
						}

						connection.release();
						resolve(true);
					},
				);
			});
		});
	}

	static async editComment(commentId, content) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const sql =
					"UPDATE posts SET content = ?, isEdited = ? WHERE id = ?";

				connection.query(
					sql,
					[content, 1, commentId],
					(err, result) => {
						if (err) {
							connection.release();
							reject(err);
							return;
						}

						connection.release();
						resolve(true);
					},
				);
			});
		});
	}

	static async getCommentOwner(commentId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const sql = "SELECT user_id FROM posts_comments WHERE id = ?";
				connection.query(sql, [commentId], async (err, results) => {
					connection.release();
					if (err) {
						reject(err);
					} else {
						const ownerId = results[0].user_id;
						try {
							const ownerRole =
								await UserService.getRole(ownerId);
							resolve({ id: ownerId, role: ownerRole });
						} catch (error) {
							reject(error);
						}
					}
				});
			});
		});
	}

	static async toggleLike(userId, commentId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const checkLikeSql =
					"SELECT * FROM posts_comments_likes WHERE user_id = ? AND comment_id = ?";
				connection.query(
					checkLikeSql,
					[userId, commentId],
					(err, results) => {
						if (err) {
							connection.release();
							reject(err);
							return;
						}

						if (results.length > 0) {
							const removeLikeSql =
								"DELETE FROM posts_comments_likes WHERE user_id = ? AND comment_id = ?";
							connection.query(
								removeLikeSql,
								[userId, commentId],
								err => {
									if (err) {
										connection.release();
										reject(err);
									} else {
										PostsCommentsService.getLikesCount(
											commentId,
										)
											.then(likesCount => {
												connection.release();
												resolve({
													likesCount,
												});
											})
											.catch(error => {
												connection.release();
												reject(error);
											});
									}
								},
							);
						} else {
							const addLikeSql =
								"INSERT INTO posts_comments_likes (user_id, comment_id) VALUES (?, ?)";
							connection.query(
								addLikeSql,
								[userId, commentId],
								err => {
									if (err) {
										connection.release();
										reject(err);
									} else {
										PostsCommentsService.getLikesCount(
											commentId,
										)
											.then(likesCount => {
												connection.release();
												resolve({
													likesCount,
												});
											})
											.catch(error => {
												connection.release();
												reject(error);
											});
									}
								},
							);
						}
					},
				);
			});
		});
	}
}

module.exports = PostsCommentsService;
