const pool = require("../database");
const Utils = require("../Utils");
const { UserRole, formatDate } = require("../../src/shared/shared");
const moment = require("moment-timezone");

class UserService {
	static async getSessionUser(userSession) {
		try {
			if (userSession) {
				return {
					login: userSession.login,
					role: await UserService.getRole(userSession.id),
				};
			} else {
				return null;
			}
		} catch (error) {
			return null;
		}
	}

	static async getRole(userId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject("Błąd pobierania połączenia!");
					return;
				}

				const sql = "SELECT level FROM admins WHERE user_id = ?";
				connection.query(sql, [userId], (error, results) => {
					connection.release();

					if (error) {
						reject("Wystąpił błąd!");
						return;
					}

					if (results.length > 0) {
						resolve(results[0].level);
					} else {
						resolve(UserRole.USER);
					}
				});
			});
		});
	}

	static async logIn(login, password) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject({
						message: "Błąd pobierania połączenia!",
					});
					return;
				}

				const sql = `
					SELECT users.id, bans.date_end
					FROM users
					LEFT JOIN bans ON users.id = bans.user_id
					WHERE users.login = ? AND users.password = ?
				`;

				connection.query(
					sql,
					[login, password],
					async (error, results) => {
						if (error) {
							connection.release();
							reject({ message: "Wystąpił błąd!" });
							return;
						}

						if (results.length > 0) {
							const userId = results[0].id;
							const banEndDate = results[0].date_end;

							if (
								banEndDate &&
								new Date(banEndDate) > new Date()
							) {
								connection.release();
								reject({
									message: `Twoje konto zostało zbanowane do: ${formatDate(banEndDate)}`,
								});
								return;
							}

							try {
								const role = await UserService.getRole(userId);
								connection.release();
								resolve({
									id: userId,
									role: role,
								});
							} catch (err) {
								connection.release();
								reject({ message: err });
							}
						} else {
							connection.release();
							reject({
								message: "Błędny login lub hasło!",
							});
						}
					},
				);
			});
		});
	}

	static async registerIn(login, email, password) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject({
						result: false,
						message: "Błąd pobierania połączenia!",
					});
					return;
				}

				const sql =
					"INSERT INTO users (login, email, password) VALUES (?, ?, ?)";
				connection.query(
					sql,
					[login, email, password],
					(error, results) => {
						if (error) {
							if (error.code === "ER_DUP_ENTRY") {
								connection.release();
								if (error.message.includes("unique_login")) {
									resolve({
										result: false,
										message: "Podany login jest zajęty!",
									});
								} else if (
									error.message.includes("unique_email")
								) {
									resolve({
										result: false,
										message: "Adres email już istnieje!",
									});
								}
								return;
							} else {
								connection.release();
								reject({
									result: false,
									message: "Wystąpił błąd!",
								});
								return;
							}
						}

						const userId = results.insertId;
						UserService.getRole(userId)
							.then(role => {
								connection.release();
								resolve({
									result: true,
									message: "",
									id: userId,
									role: role,
								});
							})
							.catch(err => {
								connection.release();
								reject({ result: false, message: err });
							});
					},
				);
			});
		});
	}

	static async getProfile(login) {
		return new Promise(async (resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(null);
					return;
				}

				const sql = `
					SELECT
						u.id,
						u.login, 
						u.created_at, 
						u.description,
						u.avatar,
						COUNT(DISTINCT p.id) AS postCount,
						COUNT(DISTINCT pc.id) AS commentCount,
						(
							SELECT COUNT(*)
							FROM posts_likes pl
							JOIN posts p ON pl.post_id = p.id
							WHERE p.user_id = u.id
						) AS postLikes,
						(
							SELECT COUNT(*)
							FROM posts_comments_likes pcl
							JOIN posts_comments pc ON pcl.comment_id = pc.id
							WHERE pc.user_id = u.id
						) AS commentLikes
					FROM 
						users u
					LEFT JOIN 
						posts p ON u.id = p.user_id
					LEFT JOIN 
						posts_comments pc ON p.id = pc.post_id
					WHERE 
						u.login = ?
					GROUP BY 
						u.id
				`;

				connection.query(sql, [login], async (error, results) => {
					connection.release();

					if (error) {
						reject(null);
						return;
					}

					if (results.length > 0) {
						resolve({
							id: results[0].id,
							login: results[0].login,
							created_at: results[0].created_at,
							description: results[0].description || null,
							postCount: results[0].postCount || 0,
							commentCount: results[0].commentCount || 0,
							totalLikes:
								(results[0].postLikes || 0) +
								(results[0].commentLikes || 0),
							avatar: Utils.convertImageToLink(results[0].avatar),
							role: await UserService.getRole(results[0].id),
						});
					} else {
						resolve(null);
					}
				});
			});
		});
	}

	static async updateDescription(userId, description) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(false);
					return;
				}

				const sql = "UPDATE users SET description = ? WHERE id = ?";
				connection.query(
					sql,
					[description, userId],
					(error, results) => {
						connection.release();

						if (error) {
							reject(false);
							return;
						}

						if (results.affectedRows > 0) {
							resolve(true);
						} else {
							resolve(false);
						}
					},
				);
			});
		});
	}

	static async updateAvatar(userId, buffer) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(false);
					return;
				}

				const sql = "UPDATE users SET avatar = ? WHERE id = ?";
				connection.query(sql, [buffer, userId], (error, results) => {
					connection.release();

					if (error) {
						reject(false);
						return;
					}

					if (results.affectedRows > 0) {
						resolve(true);
					} else {
						resolve(false);
					}
				});
			});
		});
	}

	static async getUsers() {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject("Błąd pobierania połączenia!");
					return;
				}

				const sql = `
					SELECT 
						u.id,
						u.login,
						u.email,
						u.created_at,
						u.avatar,
						IFNULL(a.level, ${UserRole.USER}) AS role,
						b.date_end AS ban_end_date
					FROM 
						users u
					LEFT JOIN
						admins a ON u.id = a.user_id
					LEFT JOIN
						bans b ON u.id = b.user_id
					GROUP BY 
						u.id
				`;

				connection.query(sql, (error, results) => {
					connection.release();

					if (error) {
						reject("Wystąpił błąd!");
						return;
					}

					const users = results.map(user => ({
						id: user.id,
						login: user.login,
						email: user.email,
						createdAt: user.created_at,
						avatar: Utils.convertImageToLink(user.avatar),
						role: user.role,
						isBanned: user.ban_end_date ? true : false,
					}));

					resolve(users);
				});
			});
		});
	}

	static async addBan(userId, grantorId, reason, dateEnd) {
		await UserService.removeBan(userId);
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject("Błąd pobierania połączenia!");
					return;
				}

				const sql = `
			  INSERT INTO bans (user_id, grantor_id, reason, date_start, date_end)
			  VALUES (?, ?, ?, ?, ?)
			`;
				connection.query(
					sql,
					[userId, grantorId, reason, moment().toDate(), dateEnd],
					(error, results) => {
						connection.release();

						if (error) {
							reject("Wystąpił błąd podczas dodawania bana!");
							return;
						}

						resolve();
					},
				);
			});
		});
	}

	static async removeBan(userId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject("Błąd pobierania połączenia!");
					return;
				}

				const sql = `DELETE FROM bans WHERE user_id = ?`;
				connection.query(sql, [userId], (error, results) => {
					connection.release();

					if (error) {
						reject("Wystąpił błąd podczas usuwania bana!");
						return;
					}

					resolve("Ban został usunięty pomyślnie!");
				});
			});
		});
	}

	static async getBanInfo(userId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject("Błąd pobierania połączenia!");
					return;
				}

				const sql = `
				SELECT 
					b.reason,
					b.date_start,
					b.date_end,
					g.login AS grantorLogin
				FROM 
					bans b
				LEFT JOIN
					users u ON b.user_id = u.id
				LEFT JOIN
					users g ON b.grantor_id = g.id
				WHERE
					b.user_id = ?`;

				connection.query(sql, [userId], (error, results) => {
					connection.release();

					if (error) {
						reject(
							"Wystąpił błąd podczas pobierania informacji o banie!",
						);
						return;
					}

					if (results.length > 0) {
						const banInfo = {
							login: results[0].login,
							reason: results[0].reason,
							dateStart: results[0].date_start,
							dateEnd: results[0].date_end,
							grantorLogin: results[0].grantorLogin,
						};
						resolve(banInfo);
					} else {
						reject("Dany użytkownik nie posiada bana!");
					}
				});
			});
		});
	}

	static async changeRole(userId, newRole) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject("Błąd pobierania połączenia!");
					return;
				}

				const deleteRoleSql = "DELETE FROM admins WHERE user_id = ?";
				connection.query(deleteRoleSql, [userId], (error, results) => {
					if (error) {
						reject("Wystąpił błąd podczas usuwania roli!");
						return;
					}

					if (newRole !== UserRole.USER) {
						const addRoleSql =
							"INSERT INTO admins (user_id, level) VALUES (?, ?)";
						connection.query(
							addRoleSql,
							[userId, newRole],
							(addRoleError, addRoleResults) => {
								connection.release();
								if (addRoleError) {
									reject(
										"Wystąpił błąd podczas dodawania nowej roli!",
									);
									return;
								}
								resolve();
							},
						);
					} else {
						connection.release();
						resolve();
					}
				});
			});
		});
	}
}

module.exports = UserService;
