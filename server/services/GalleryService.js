const pool = require("../database");
const moment = require("moment-timezone");
const UserService = require("./UserService");
const Utils = require("../Utils");

class GalleryService {
	static maxPicturesPerPage = 5;

	static async addPicture(title, file, userId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const sql =
					"INSERT INTO pictures (title, file, user_id, created_at) VALUES (?, ?, ?, ?)";

				connection.query(
					sql,
					[title, file, userId, moment().toDate()],
					(err, result) => {
						connection.release();
						if (err) {
							reject(err);
						} else {
							resolve(result.insertId);
						}
					},
				);
			});
		});
	}

	static async getPictures(pageId) {
		return new Promise((resolve, reject) => {
			const pictures = [];
			let hasNextPage = false;

			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const offset = pageId * GalleryService.maxPicturesPerPage;

				connection.query(
					"SELECT pictures.id, pictures.title, pictures.file, users.id AS authorId, users.login AS author, users.avatar, pictures.created_at " +
						"FROM pictures " +
						"INNER JOIN users ON pictures.user_id = users.id " +
						"ORDER BY pictures.id DESC " +
						"LIMIT ?, ?",
					[offset, GalleryService.maxPicturesPerPage + 1],
					async (err, results) => {
						if (err) {
							connection.release();
							reject(err);
							return;
						}

						const picturePromises = results.map(
							async (result, index) => {
								if (
									index === GalleryService.maxPicturesPerPage
								) {
									hasNextPage = true;
									return;
								}

								const picture = {
									id: result.id,
									title: result.title,
									file: Utils.convertImageToLink(result.file),
									author: result.author,
									avatar: Utils.convertImageToLink(
										result.avatar,
									),
									authorRole: await UserService.getRole(
										result.authorId,
									),
									createdAt: result.created_at,
								};

								pictures.push(picture);
							},
						);

						Promise.all(picturePromises)
							.then(() => {
								connection.release();
								resolve({
									pictures,
									hasNextPage,
								});
							})
							.catch(error => {
								connection.release();
								reject(error);
							});
					},
				);
			});
		});
	}
}

module.exports = GalleryService;
