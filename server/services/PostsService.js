const pool = require("../database");
const moment = require("moment-timezone");
const UserService = require("./UserService");
const Utils = require("../Utils");

class PostsService {
	static maxPostsPerPage = 5;

	static async getLikesCount(postId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				connection.query(
					"SELECT COUNT(user_id) AS likeCount FROM posts_likes WHERE post_id = ?",
					[postId],
					(err, results) => {
						connection.release();
						if (err) {
							reject(err);
						} else {
							resolve(results[0].likeCount);
						}
					},
				);
			});
		});
	}

	static async getCommentsCount(postId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				connection.query(
					"SELECT COUNT(user_id) AS commentsCount FROM posts_comments WHERE post_id = ?",
					[postId],
					(err, results) => {
						connection.release();
						if (err) {
							reject(err);
						} else {
							resolve(results[0].commentsCount);
						}
					},
				);
			});
		});
	}

	static async getPosts(pageId, tags, login = null) {
		return new Promise((resolve, reject) => {
			const posts = [];
			let hasNextPage = false;

			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const offset = pageId * PostsService.maxPostsPerPage;
				const args = [];
				let sql =
					"SELECT posts.id, posts.title, posts.content, posts.isEdited, users.id AS authorId, users.login AS author, posts.created_at, GROUP_CONCAT(posts_tags.tag_name) AS tags, users.avatar " +
					"FROM posts " +
					"INNER JOIN users ON posts.user_id = users.id " +
					"LEFT JOIN posts_tags ON posts.id = posts_tags.post_id ";

				if (login !== null) {
					sql += "WHERE users.login = ? ";
					args.push(login);
				} else if (tags && tags.length > 0) {
					sql += "WHERE posts_tags.tag_name IN (?) ";
					args.push(tags);
				}

				sql +=
					"GROUP BY posts.id " +
					"ORDER BY posts.id DESC " +
					"LIMIT ?, ?";

				args.push(offset, PostsService.maxPostsPerPage + 1);

				connection.query(sql, args, async (err, results) => {
					if (err) {
						connection.release();
						reject(err);
						return;
					}

					const postPromises = results.map(async (result, index) => {
						if (index === PostsService.maxPostsPerPage) {
							hasNextPage = true;
							return;
						}

						const post = {
							id: result.id,
							title: result.title,
							content: result.content,
							author: result.author,
							createdAt: result.created_at,
							avatar: Utils.convertImageToLink(result.avatar),
							authorRole: await UserService.getRole(
								result.authorId,
							),
							isEdited: result.isEdited === 1,
						};

						post.likesCount = await PostsService.getLikesCount(
							result.id,
						);
						post.commentsCount =
							await PostsService.getCommentsCount(result.id);

						post.tags = result.tags ? result.tags.split(",") : [];

						return post;
					});

					Promise.all(postPromises)
						.then(postsArray => {
							connection.release();
							const filteredPosts = postsArray.filter(
								post => post !== undefined,
							);
							resolve({
								posts: filteredPosts,
								hasNextPage,
							});
						})
						.catch(error => {
							connection.release();
							reject(error);
						});
				});
			});
		});
	}

	static async addPost(title, content, words, userId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const sql =
					"INSERT INTO posts (title, content, user_id, created_at) VALUES (?, ?, ?, ?)";

				connection.beginTransaction(async err => {
					if (err) {
						connection.release();
						reject(err);
						return;
					}

					try {
						const postResult = await new Promise(
							(resolve, reject) => {
								connection.query(
									sql,
									[title, content, userId, moment().toDate()],
									(err, result) => {
										if (err) {
											reject(err);
											return;
										}
										resolve(result);
									},
								);
							},
						);

						const postId = postResult.insertId;
						if (words && words.length > 0) {
							const insertTagsPromises = words.map(async tag => {
								await new Promise((resolve, reject) => {
									const tagSql =
										"INSERT INTO posts_tags (tag_name, post_id) VALUES (?, ?)";
									connection.query(
										tagSql,
										[tag, postId],
										(err, result) => {
											if (err) {
												reject(err);
												return;
											}
											resolve(result);
										},
									);
								});
							});

							await Promise.all(insertTagsPromises);
						}

						connection.commit(err => {
							if (err) {
								connection.rollback(() => {
									connection.release();
									reject(err);
								});
								return;
							}
							connection.release();
							resolve(true);
						});
					} catch (err) {
						connection.rollback(() => {
							connection.release();
							reject(err);
						});
					}
				});
			});
		});
	}

	static async editPost(postId, title, content, words) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const sql =
					"UPDATE posts SET title = ?, content = ?, isEdited = ? WHERE id = ?";

				connection.beginTransaction(async err => {
					if (err) {
						connection.release();
						reject(err);
						return;
					}

					try {
						await new Promise((resolve, reject) => {
							connection.query(
								sql,
								[title, content, 1, postId],
								(err, result) => {
									if (err) {
										reject(err);
										return;
									}
									resolve(result);
								},
							);
						});

						if (words && words.length > 0) {
							const deleteTagsSql =
								"DELETE FROM posts_tags WHERE post_id = ?";
							await new Promise((resolve, reject) => {
								connection.query(
									deleteTagsSql,
									[postId],
									(err, result) => {
										if (err) {
											reject(err);
											return;
										}
										resolve(result);
									},
								);
							});

							const insertTagsPromises = words.map(async tag => {
								await new Promise((resolve, reject) => {
									const tagSql =
										"INSERT INTO posts_tags (tag_name, post_id) VALUES (?, ?)";
									connection.query(
										tagSql,
										[tag, postId],
										(err, result) => {
											if (err) {
												reject(err);
												return;
											}
											resolve(result);
										},
									);
								});
							});

							await Promise.all(insertTagsPromises);
						}

						connection.commit(err => {
							if (err) {
								connection.rollback(() => {
									connection.release();
									reject(err);
								});
								return;
							}
							connection.release();
							resolve(true);
						});
					} catch (err) {
						connection.rollback(() => {
							connection.release();
							reject(err);
						});
					}
				});
			});
		});
	}

	static async getPostsOwner(postId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const sql = "SELECT user_id FROM posts WHERE id = ?";
				connection.query(sql, [postId], async (err, results) => {
					connection.release();
					if (err) {
						reject(err);
					} else {
						const ownerId = results[0].user_id;
						try {
							const ownerRole =
								await UserService.getRole(ownerId);
							resolve({ id: ownerId, role: ownerRole });
						} catch (error) {
							reject(error);
						}
					}
				});
			});
		});
	}

	static async toggleLike(userId, postId) {
		return new Promise((resolve, reject) => {
			pool.getConnection((err, connection) => {
				if (err) {
					reject(err);
					return;
				}

				const checkLikeSql =
					"SELECT * FROM posts_likes WHERE user_id = ? AND post_id = ?";
				connection.query(
					checkLikeSql,
					[userId, postId],
					(err, results) => {
						if (err) {
							connection.release();
							reject(err);
							return;
						}

						if (results.length > 0) {
							const removeLikeSql =
								"DELETE FROM posts_likes WHERE user_id = ? AND post_id = ?";
							connection.query(
								removeLikeSql,
								[userId, postId],
								err => {
									if (err) {
										connection.release();
										reject(err);
									} else {
										PostsService.getLikesCount(postId)
											.then(likesCount => {
												connection.release();
												resolve({
													likesCount,
												});
											})
											.catch(error => {
												connection.release();
												reject(error);
											});
									}
								},
							);
						} else {
							const addLikeSql =
								"INSERT INTO posts_likes (user_id, post_id) VALUES (?, ?)";
							connection.query(
								addLikeSql,
								[userId, postId],
								err => {
									if (err) {
										connection.release();
										reject(err);
									} else {
										PostsService.getLikesCount(postId)
											.then(likesCount => {
												connection.release();
												resolve({
													likesCount,
												});
											})
											.catch(error => {
												connection.release();
												reject(error);
											});
									}
								},
							);
						}
					},
				);
			});
		});
	}
}

module.exports = PostsService;
