-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 16, 2024 at 01:46 PM
-- Wersja serwera: 10.4.28-MariaDB
-- Wersja PHP: 8.2.4
SET
  SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

START TRANSACTION;

SET
  time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */
;

/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */
;

/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */
;

/*!40101 SET NAMES utf8mb4 */
;

--
-- Database: `memeact`
--
-- --------------------------------------------------------
--
-- Struktura tabeli dla tabeli `admins`
--
CREATE TABLE `admins` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `level` tinyint(4) NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- --------------------------------------------------------
--
-- Struktura tabeli dla tabeli `bans`
--
CREATE TABLE `bans` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `date_start` datetime DEFAULT NULL,
  `date_end` datetime DEFAULT NULL,
  `grantor_id` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- --------------------------------------------------------
--
-- Struktura tabeli dla tabeli `pictures`
--
CREATE TABLE `pictures` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `file` longblob NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- --------------------------------------------------------
--
-- Struktura tabeli dla tabeli `posts`
--
CREATE TABLE `posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `isEdited` tinyint(4) NOT NULL DEFAULT 0
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- --------------------------------------------------------
--
-- Struktura tabeli dla tabeli `posts_comments`
--
CREATE TABLE `posts_comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL,
  `content` text NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `isEdited` tinyint(4) NOT NULL DEFAULT 0
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- --------------------------------------------------------
--
-- Struktura tabeli dla tabeli `posts_comments_likes`
--
CREATE TABLE `posts_comments_likes` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `comment_id` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- --------------------------------------------------------
--
-- Struktura tabeli dla tabeli `posts_likes`
--
CREATE TABLE `posts_likes` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- --------------------------------------------------------
--
-- Struktura tabeli dla tabeli `posts_tags`
--
CREATE TABLE `posts_tags` (
  `tag_name` varchar(20) NOT NULL,
  `post_id` int(10) UNSIGNED NOT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

-- --------------------------------------------------------
--
-- Struktura tabeli dla tabeli `users`
--
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `login` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `email` varchar(50) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `description` text DEFAULT NULL,
  `avatar` longblob DEFAULT NULL
) ENGINE = InnoDB DEFAULT CHARSET = utf8mb4 COLLATE = utf8mb4_general_ci;

--
-- Indeksy dla zrzutów tabel
--
--
-- Indeksy dla tabeli `admins`
--
ALTER TABLE
  `admins`
ADD
  PRIMARY KEY (`user_id`);

--
-- Indeksy dla tabeli `bans`
--
ALTER TABLE
  `bans`
ADD
  KEY `user_id` (`user_id`),
ADD
  KEY `bans_ibfk_2` (`grantor_id`);

--
-- Indeksy dla tabeli `pictures`
--
ALTER TABLE
  `pictures`
ADD
  PRIMARY KEY (`id`),
ADD
  KEY `fk_user_id` (`user_id`);

--
-- Indeksy dla tabeli `posts`
--
ALTER TABLE
  `posts`
ADD
  PRIMARY KEY (`id`),
ADD
  KEY `fk_user_id` (`user_id`);

--
-- Indeksy dla tabeli `posts_comments`
--
ALTER TABLE
  `posts_comments`
ADD
  PRIMARY KEY (`id`),
ADD
  KEY `posts_comments_user_id_2_users_id` (`user_id`);

--
-- Indeksy dla tabeli `posts_comments_likes`
--
ALTER TABLE
  `posts_comments_likes`
ADD
  PRIMARY KEY (`user_id`, `comment_id`),
ADD
  KEY `posts_comments_likes_2_posts_comments` (`comment_id`);

--
-- Indeksy dla tabeli `posts_likes`
--
ALTER TABLE
  `posts_likes`
ADD
  PRIMARY KEY (`user_id`, `post_id`),
ADD
  KEY `posts_likes_2_posts` (`post_id`);

--
-- Indeksy dla tabeli `posts_tags`
--
ALTER TABLE
  `posts_tags`
ADD
  PRIMARY KEY (`tag_name`, `post_id`),
ADD
  KEY `posts_tags_post_id_2_posts_id` (`post_id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE
  `users`
ADD
  PRIMARY KEY (`id`),
ADD
  UNIQUE KEY `unique_login` (`login`),
ADD
  UNIQUE KEY `unique_email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--
--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE
  `pictures`
MODIFY
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE
  `posts`
MODIFY
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts_comments`
--
ALTER TABLE
  `posts_comments`
MODIFY
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE
  `users`
MODIFY
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--
--
-- Constraints for table `admins`
--
ALTER TABLE
  `admins`
ADD
  CONSTRAINT `admins_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `bans`
--
ALTER TABLE
  `bans`
ADD
  CONSTRAINT `bans_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `bans_ibfk_2` FOREIGN KEY (`grantor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pictures`
--
ALTER TABLE
  `pictures`
ADD
  CONSTRAINT `fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE
  `posts`
ADD
  CONSTRAINT `fk_posts_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `posts_comments`
--
ALTER TABLE
  `posts_comments`
ADD
  CONSTRAINT `posts_comments_user_id_2_users_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `posts_comments_likes`
--
ALTER TABLE
  `posts_comments_likes`
ADD
  CONSTRAINT `posts_comments_likes_2_posts_comments` FOREIGN KEY (`comment_id`) REFERENCES `posts_comments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `posts_comments_likes_2_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts_likes`
--
ALTER TABLE
  `posts_likes`
ADD
  CONSTRAINT `posts_likes_2_posts` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD
  CONSTRAINT `posts_likes_2_users` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `posts_tags`
--
ALTER TABLE
  `posts_tags`
ADD
  CONSTRAINT `posts_tags_post_id_2_posts_id` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */
;

/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */
;

/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */
;